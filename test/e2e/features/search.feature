Feature: Searching on Wikipedia
  As a User,
  I want to find information by a relevant keyword,
  In order to easily find entries without having to browse each page by hand

Scenario: A perfectly matching entry is found
  Given I am on the English Wikipedia main page
  When I search for "Socrates"
  Then I should see the entry with the title "Socrates"

Scenario: No possible matching entry is found
  Given I am on the English Wikipedia main page
  When I search for "Socratasty"
  Then I should see the search results page
  And I should not see a list of search results

Scenario Outline: Multiple possible matching entries are found
  Given I am on the English Wikipedia main page
  When I search for "<search term>"
  Then I should see the search results page
  And the search results should contain "<entry>"

  Examples:
    | search term       | entry                    |
    | Socratez          | Socrates                 |
    | Pllato            | Plato                    |
    | Greece Philosophy | Ancient Greek philosophy |