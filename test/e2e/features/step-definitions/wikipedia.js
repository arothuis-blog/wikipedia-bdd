const Nightmare = require("nightmare");
const { expect } = require("chai");
const { BeforeAll, Given, When, Then, AfterAll } = require("cucumber");

let browser;
BeforeAll(function () {
    browser = Nightmare({
        show: false
    });
});

const ENGLISH_WIKIPEDIA = "https://en.wikipedia.org";
const SEARCH_RESULTS_TITLE = "Search results";

Given("I am on the English Wikipedia main page", function () {
    return browser.goto(ENGLISH_WIKIPEDIA);
});

When("I search for {string}", function (searchTerm) {
    return browser
        .insert("#searchInput", searchTerm)
        .click("#searchButton")
        .wait(250);
});

Then("I should see the entry with the title {string}", function (expectedTitle) {
    return browser
        .evaluate(() => document.getElementById("firstHeading").textContent.trim())
        .then((titleText) => expect(titleText).to.equal(expectedTitle));
});

Then("I should see the search results page", function () {
    return browser
        .evaluate(() => document.getElementById("firstHeading").textContent.trim())
        .then((titleText) => expect(titleText).to.equal(SEARCH_RESULTS_TITLE))
});

Then("I should not see a list of search results", function () {
    return browser
        .evaluate(() => document.getElementsByClassName("mw-search-results").length > 0)
        .then((hasSearchResults) => expect(hasSearchResults).to.be.false);
});

Then('the search results should contain {string}', function (expectedMatch) {
    return browser
        .evaluate(() => {
            return Array.from(document.getElementsByClassName("mw-search-result-heading"))
                .map(element => element.textContent.trim());
        })
        .then((resultHeadings) => expect(resultHeadings).to.contain(expectedMatch));
});

AfterAll(function () {
    return browser.end();
});